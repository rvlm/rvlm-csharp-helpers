Reflection helper for Microsoft .NET v. 2.0
===========================================
**Author:** [Pavel Kretov](mailto:firegurafiku@gmail.com)  
**License:** [MIT License](http://en.wikipedia.org/wiki/MIT_License)  
**Language:** C#

This project contains some helper routines for features which are absent from .NET version 2.0,
in class libraries or runtime. For example, *PropertyHelper* class was written to make it
possible to retrieve reflection *PropertyInfo* object for a property without having to hardcode
its name in a string. Visit project wiki for more information.
