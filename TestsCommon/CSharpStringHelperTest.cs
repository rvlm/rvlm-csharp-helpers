﻿using System;
using System.Collections.Generic;
using System.Text;
using Rvlm.CSharpHelpers.Extensions;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Rvlm.UnitTests.CSharpHelpers
{
    [TestFixture]
    public class CSharpStringHelperTest
    {
        private void TestIdentity(string str)
        {
            Assert.AreEqual(str, str.EscapeCSharpString().UnescapeCSharpString());
        }

        [Test]
        public void ThereAndBackAgain()
        {
            TestIdentity("One right to rule them all.");
            TestIdentity("\"\"\"\'\\\0\a\b\f\t\n\r\v\"\"\"");
            TestIdentity("\u4fda\u1234\u6421\u3451\u1248\u9024\u9213\u4823\u1234");
        }

        [Test]
        public void Excape()
        {
            Assert.AreEqual("1\\n\\t2", "1\n\t2".EscapeCSharpString());
            Assert.AreEqual(
                "\\\"\\\'\\0\\t\\v\\n\\r\\f\\a\\b\\\\",
                "\"\'\0\t\v\n\r\f\a\b\\".EscapeCSharpString());
        }

        [Test]
        public void Unescape()
        {
            Assert.AreEqual("1\n\t2", "1\\n\\t2".UnescapeCSharpString());
            Assert.AreEqual(
                "\"\'\0\t\v\n\r\f\a\b\\",
                "\\\"\\\'\\0\\t\\v\\n\\r\\f\\a\\b\\\\".UnescapeCSharpString());
            Assert.Throws(typeof(FormatException), ()=>"12\\".UnescapeCSharpString());
            Assert.Throws(typeof(FormatException), ()=>"1\\q2".UnescapeCSharpString());
            Assert.Throws(typeof(FormatException), ()=>"1\\u013".UnescapeCSharpString());
            Assert.Throws(typeof(FormatException), ()=>"1\\u".UnescapeCSharpString());
            Assert.Throws(typeof(FormatException), ()=>"1\\u0afr".UnescapeCSharpString());
        }
    }
}
