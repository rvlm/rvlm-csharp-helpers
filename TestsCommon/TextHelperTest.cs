﻿using System;
using System.Collections.Generic;
using Rvlm.CSharpHelpers.Extensions;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Rvlm.UnitTests.CSharpHelpers
{
    [TestFixture]
    public class TextHelperTest
    {
        private static readonly byte[] data = { 0xfe, 0x00, 0x5a, 0x49, 0x42, 0x77 };
        private static readonly string result = "fe005a494277";

        [Test]
        public void EncodeToHex()
        {
            Assert.AreEqual(result, TextHelper.EncodeToHex((IEnumerable<byte>)data));
            Assert.AreEqual(result, TextHelper.EncodeToHex((IList<byte>)data));
        }

        [Test]
        public void DecodeFromHex()
        {
            byte[] buf = TextHelper.DecodeFromHex(result);
            Assert.AreEqual(data.Length, buf.Length);
            Assert.AreEqual(data[0], buf[0]);
            Assert.AreEqual(data[1], buf[1]);
            Assert.AreEqual(data[2], buf[2]);
            Assert.AreEqual(data[3], buf[3]);
            Assert.AreEqual(data[4], buf[4]);
            Assert.AreEqual(data[5], buf[5]);
        }
    }
}
