﻿using System;
using System.Collections.Generic;
using Rvlm.CSharpHelpers.Extensions;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Rvlm.UnitTests.CSharpHelpers
{
    [TestFixture]
    public class StringHelperTest
    {
        [Test]
        public void StartWith()
        {
            Assert.IsTrue ("Alphabet".StartsWith('A'));
            Assert.IsFalse("Chinese alphabet".StartsWith('A'));
            Assert.IsFalse("".StartsWith('X'));
        }

        [Test]
        public void EndsWith()
        {
            Assert.IsFalse("Alphabet".EndsWith('A'));
            Assert.IsTrue ("Chinese alphabet".EndsWith('t'));
            Assert.IsFalse("".EndsWith('X'));
        }

        [Test]
        public void Contains()
        {
            Assert.IsTrue("".Contains("", 0));
            Assert.IsFalse("".Contains("Everything"));
            Assert.IsTrue("Hello!".Contains("Hell", 0));
            Assert.IsTrue("Earth".Contains("art", 1));
            Assert.IsFalse("Hitchhike".Contains("42", 3));
        }

        [Test]
        public void ReplaceMultiple()
        {
            Assert.AreEqual(
                "",
                "".ReplaceMultiple(
                    "1", "2",
                    "2", "3",
                    "3", "3"));

            Assert.AreEqual(
                "1 2 3",
                "1 2 3".ReplaceMultiple(
                    "1", "1",
                    "2", "2",
                    "3", "3"));

            Assert.AreEqual(
                "one two three",
                "1 2 3".ReplaceMultiple(
                    "1", "one",
                    "2", "two",
                    "3", "three"));

            Assert.AreEqual(
                "312",
                "123".ReplaceMultiple(
                    "1", "3",
                    "2", "1",
                    "3", "2"));

            Assert.AreEqual(
                "91134",
                "123".ReplaceMultiple(
                    "1", "911",
                    "2", "34",
                    "3", ""));
        }
    }
}
