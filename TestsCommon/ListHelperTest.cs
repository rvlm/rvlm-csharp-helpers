﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Rvlm.CSharpHelpers.Extensions;
using Assert = NUnit.Framework.Assert;

namespace Rvlm.UnitTests.CSharpHelpers
{
    [TestFixture]
    public class ListHelperTest
    {
        [Test]
        public void AddAndRemove()
        {
            List<int> list = new List<int>();

            list.BinarySearchAdd(5);
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(5, list[0]);

            list.BinarySearchAdd(1);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(5, list[1]);

            list.BinarySearchAdd(3);
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
            Assert.AreEqual(5, list[2]);

            list.BinarySearchAdd(2);
            Assert.AreEqual(4, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
            Assert.AreEqual(5, list[3]);

            list.BinarySearchAdd(4);
            Assert.AreEqual(5, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
            Assert.AreEqual(4, list[3]);
            Assert.AreEqual(5, list[4]);

            list.BinarySearchRemove(4);
            Assert.AreEqual(4, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(3, list[2]);
            Assert.AreEqual(5, list[3]);

            list.BinarySearchRemove(2);
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(3, list[1]);
            Assert.AreEqual(5, list[2]);

            list.BinarySearchRemove(3);
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(5, list[1]);

            list.BinarySearchRemove(1);
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(5, list[0]);

            list.BinarySearchRemove(5);
            Assert.AreEqual(0, list.Count);
        }

        [Test]
        public void SearchBy()
        {
            Func<string,int> map = str => str.Length;
            Comparison<string> comparison = (a, b) => a.Length.CompareTo(b.Length);
            List<string> list = new List<string>() {
                "@@",         // 0
                "@@@@",       // 1
                "@@@@@@",     // 2
                "@@@@@@@@",   // 3
                "@@@@@@@@@@", // 4
            };
            
            int c = list.Count;
            Assert.AreEqual(~0, list.BinarySearchBy(map, 0));
            Assert.AreEqual(~0, list.BinarySearchBy(map, 1));
            Assert.AreEqual( 0, list.BinarySearchBy(map, 2));
            Assert.AreEqual(~1, list.BinarySearchBy(map, 3));
            Assert.AreEqual( 1, list.BinarySearchBy(map, 4));
            Assert.AreEqual(~2, list.BinarySearchBy(map, 5));
            Assert.AreEqual( 2, list.BinarySearchBy(map, 6));
            Assert.AreEqual(~3, list.BinarySearchBy(map, 7));
            Assert.AreEqual( 3, list.BinarySearchBy(map, 8));
            Assert.AreEqual(~4, list.BinarySearchBy(map, 9));
            Assert.AreEqual( 4, list.BinarySearchBy(map, 10));
            Assert.AreEqual(~c, list.BinarySearchBy(map, 11));
            Assert.AreEqual(~c, list.BinarySearchBy(map, 12));
        }       
    }
}
