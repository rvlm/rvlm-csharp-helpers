﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Rvlm.UnitTests.CSharpHelpers
{
    using ObjectTextConverter = Rvlm.CSharpHelpers.ObjectTextConverter;
    using ObjectTextConverterException = Rvlm.CSharpHelpers.ObjectTextConverterException;

    [TestFixture]
    public class ObjectTextConverterTest
    {
        private string Format(object obj, Type type)
        {
            return ObjectTextConverter.ConvertToString(obj, type);
        }

        private object Parse(string str, Type type)
        {
            return ObjectTextConverter.ConvertFromString(str, type);
        }

        private void TestValueAndNullable<T>(T value)
            where T : struct
        {
            string str = Format(value, value.GetType());
            object newValue = Parse(str, value.GetType());

            Assert.AreEqual(value, newValue);
            Assert.AreEqual(value.GetType(), newValue.GetType());

            var nullableValue = new Nullable<T>(value);
            var nullableType = nullableValue.GetType();
            var newNullableValue = Parse(
                Format(nullableValue, nullableType), nullableType);
            var newNullableType = newNullableValue.GetType();
            Assert.AreEqual(nullableValue, newNullableValue);
            Assert.AreEqual(nullableType, newNullableType);
        }

        private void TestValue<T>(T value)
        {
            string str = Format(value, value.GetType());
            object newValue = Parse(str, value.GetType());
            Assert.AreEqual(value, newValue);
            Assert.AreEqual(value.GetType(), newValue.GetType());
        }

        private enum TestEnum
        {
            Item1,
            Item2,
            Item3,
        }

        [Test]
        public void DateTimeRoundTrip()
        {
            var ci = System.Globalization.CultureInfo.InvariantCulture;
            var time = DateTime.Now;
            var str = time.ToString("O", ci);
            var time2 = DateTime.ParseExact(str, "O", ci);
            Assert.AreEqual(time, time2);
        }

        [Test]
        public void ThereAndBackAgain()
        {
            TestValueAndNullable(true);
            TestValueAndNullable(false);

            TestValueAndNullable(42);
            TestValueAndNullable(int.MinValue);
            TestValueAndNullable(int.MaxValue);
            TestValueAndNullable(short.MinValue);
            TestValueAndNullable(short.MaxValue);
            TestValueAndNullable(long.MinValue);
            TestValueAndNullable(long.MaxValue);
            TestValueAndNullable(uint.MinValue);
            TestValueAndNullable(uint.MaxValue);
            TestValueAndNullable(ushort.MinValue);
            TestValueAndNullable(ushort.MaxValue);
            TestValueAndNullable(ulong.MinValue);
            TestValueAndNullable(ulong.MaxValue);

            TestValueAndNullable(float.MinValue);
            TestValueAndNullable(float.MaxValue);
            TestValueAndNullable(float.Epsilon);
            TestValueAndNullable(float.PositiveInfinity);
            TestValueAndNullable(float.NegativeInfinity);
            Assert.That(float.IsNaN(
                (float)Parse(Format(float.NaN, typeof(float)), typeof(float))));
            TestValueAndNullable(double.MinValue);
            TestValueAndNullable(double.MaxValue);
            TestValueAndNullable(double.Epsilon);
            TestValueAndNullable(double.PositiveInfinity);
            TestValueAndNullable(double.NegativeInfinity);
            Assert.That(double.IsNaN(
                (double)Parse(Format(double.NaN, typeof(double)), typeof(double))));
            
            TestValueAndNullable('A');
            TestValueAndNullable(char.MinValue);
            TestValueAndNullable(char.MaxValue);

            TestValue("AaaaAAaaaasaAAAAA");
            TestValue("");
            TestValue("\"\"");
            TestValue("\n\t\\");

            TestValueAndNullable(DateTime.MinValue);
            TestValueAndNullable(DateTime.MaxValue);
            TestValueAndNullable(DateTime.Now);
            TestValueAndNullable(DateTime.UtcNow);

            TestValue(new Version("1.2.3.4"));
            TestValue(new Version("1.2.3"));
            TestValue(new Version("1.2"));
            TestValue(new Version("1"));

            TestValueAndNullable(TestEnum.Item1);
            TestValueAndNullable(TestEnum.Item3);

            TestValue(new List<int>(){1, -2, 3, -5});
            TestValue(new int[] {-1, 2, -4, 5});
            TestValue(new List<int?> { null, -2, 4, null, 8, 1});
            TestValue(new int?[] { null, -2, 4, 5, null, 1});
        }

    }
}
