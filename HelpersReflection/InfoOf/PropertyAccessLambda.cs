﻿using System;

namespace Rvlm.CSharpHelpers.InfoOf
{
    /**
     * Lambda expression which accesses some property value.
     * The only one thing this lambda must do is to access an instance property of some
     * class in such a way that the property getter is the only one method to be called.
     * Some examples:
     * @code
     *     PropertyAccessLambda<string,int> lambda = s => s.Length;        // CORRECT
     *     PropertyAccessLambda<string,int> lambda = s => s.Length + 1;    // INCORRECT
     *     PropertyAccessLambda<string,int> lambda = s => 1;               // INCORRECT
     *     PropertyAccessLambda<string,string> lambda = s => string.Empty; // INCORRECT
     * @endcode
     * In the case an incorrect lambda expression is given to @c InfoOf,
     * nothing is guaranteed. Most likely, an expression will be thrown. Note that there
     * is no way for the compiler to check your lambda is correct in the above sense.
     * @see InfoOf.GetPropertyInfo
     * @see InfoLookupException
     */
    public delegate TRet PropertyAccessLambda<T, TRet>(T obj);
}
