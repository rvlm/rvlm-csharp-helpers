﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Rvlm.CSharpHelpers.InfoOf
{
    using OpCodesHash = Dictionary<short, OpCodeStruct>;

    /**
     * @internal
     * Structure to be stored in hashtable of all known opcodes. This is necessary
     * because it doesn't seem to be practical to calculate operand size for every single
     * CIL instuction while parsing.
     */
    internal struct OpCodeStruct
    {
        public OpCode OpCode;
        public short OperandSize;
    }

    /**
     * @internal
     * Simple parser of opcodes sequences. It doesn't do any complex analysis, but only
     * parses individual opcodes and their arguments. It takes the method IL code as
     * constructor arguments and allows you to iterate over each opcode in the method.
     * This class doesn't implement @c IEnumerator or @c IEnumerable interface in order
     * to reduce the number of memory allocations (as we would have to pack each opcode
     * and its operand into a struct) and overall performance (as we would have to parse
     * each opcode's operand regardless it will be usefull or not). So, the usage pattern
     * is as follows:
     * @code
     *     OpCodeParser parser = new OpCodeParser(ilcode);
     *     while (parser.NextOpCode())
     *     {
     *         if (parser.CurrentOpCode == OpCodes.Callvirt)
     *         {
     *             Int32 token = parser.CurrentOperandInt32;
     *             ...
     *         }
     *     }
     * @endcode 
     * An instance of @c OpCodeParserException will be thrown on errors. No other
     * exception class is used, besides @c ArgumentNullException which may be thrown
     * by constructor.
     * 
     * @note
     * Parsing of IL assembler @c switch opcode is not fully supported yet. This class
     * will parse them successfully but without providing information about labels.
     * 
     * @note
     * Some code in this class is based on Albahari's "C# 3.0 in a Nutshell" example
     * (see the last one on page http://www.albahari.com/nutshell/ch17.aspx). The most
     * important points are commented in implementation.
     */
    internal class OpCodeParser
    {
        private static OpCodesHash mOpCodesHash = CreateOpCodesHash();
        private byte[] mCodeBuffer;
        private int mPosition;

        /**
         * Special value representing indeterminate operand size.
         * This is used in the case of @em switch instruction which takes a various
         * number of @c Int32 operands.
         */
        public const short IndeterminateOperandSize = -1;

        /**
         * Creates new class instance and loads @a ilcode into parser.
         * Constructor doen not copy its argument into new buffer, so, generally speaking,
         * it mustn't be modified while parsing is done. Parsing itself doesn't modify
         * this array too. Also, there is currently no way to reset parser to initial
         * state, so parsing can be done only once in the object's lifetime. Note that
         * an instance of @c OpCodeParserException will be thrown if @a ilcode is @c null.
         * @see NextOpCode
         * @see CurrentOpCode
         */
        public OpCodeParser(byte[] ilcode)
        {
            if (ilcode == null)
                throw new OpCodeParserException("Cannot proceed with null ilcode.");

            mCodeBuffer = ilcode;
            mPosition = 0;
        }

        /**
         * Gets the opcode of the CIL instuction being processed right now.
         * In case parsing isn't started yet, the @c default(OpCode) is returned.
         * For performance reasons this member is left as a field.
         * @see CurrentOperandSize
         * @see GetCurrentOperandInt32
         */
        public OpCode CurrentOpCode;

        /**
         * Gets the operand size of the CIL instruction beeing processed right now.
         * In case parsing isn't started yet, zero value is returned. If current opcode
         * is a @em switch one, then the special value @c IndeterminateOperandSize is
         * returned. For performance reasons this member is left as a field.
         * @see CurrentOpCode
         * @see GetCurrentOperandInt32
         */
        public short CurrentOperandSize;

        /**
         * Holds the operand value for the CIL instruction being processed right now,
         * but only if that instruction's opcode takes @c Byte operand. Otherwise this
         * field value is undefined. For performance reasons this member is left as
         * a field.
         * @see CurrentOperandSize
         * @see CurrentOperandInt16
         * @see CurrentOperandInt32
         * @see CurrentOperandInt64
         */
        public Byte CurrentOperandInt8;

        /**
         * Holds the operand value for the CIL instruction being processed right now,
         * but only if that instruction's opcode takes @c Int16 operand. Otherwise this
         * field value is undefined. For performance reasons this member is left as
         * a field.
         * @see CurrentOperandSize
         * @see CurrentOperandInt8
         * @see CurrentOperandInt32
         * @see CurrentOperandInt64
         */
        public Int16 CurrentOperandInt16;

        /**
         * Holds the operand value for the CIL instruction being processed right now,
         * but only if that instruction's opcode takes @c Int32 operand. Otherwise this
         * field value is undefined. For performance reasons this member is left as
         * a field.
         * @see CurrentOperandSize
         * @see CurrentOperandInt8
         * @see CurrentOperandInt16
         * @see CurrentOperandInt64
         */
        public Int32 CurrentOperandInt32;

        /**
         * Holds the operand value for the CIL instruction being processed right now,
         * but only if that instruction's opcode takes @c Int64 operand. Otherwise this
         * field value is undefined. For performance reasons this member is left as
         * a field.
         * @see CurrentOperandSize
         * @see CurrentOperandInt8
         * @see CurrentOperandInt16
         * @see CurrentOperandInt32
         */
        public Int64 CurrentOperandInt64;

        /**
         * Returs parser to the very beginning of the code.
         * This can be safely used to perform successive code scans.
         */
        public void Reset()
        {
            mPosition = 0;
        }

        /**
         * Advances parser to next CIL instruction in the code.
         * Returns @c true when successfully moved to next instruction, @c false when
         * the no more instructions left, and throws @c OpCodeParserException on errors.
         * Even if @c OpCodeParserException was thrown, the object is kept in consistent
         * state (but it doesn't seem to be very usefull).
         */
        public bool NextOpCode()
        {
            if (mPosition == mCodeBuffer.Length)
                return false;
            else
            {
                ReadOpCodeAndOperand();
                return true;
            }
        }

        /**
         * @internal
         * Parses opcode and it's operand. If succeedes, advances current position to the
         * next opcode and sets @c CurrentOpCode and @c CurrentOperandSize properties.
         * If fails, throws exceptions without changing current position.
         */
        private void ReadOpCodeAndOperand()
        {
            // We must save old position in order to keep the object
            // in consistent state in case errors occur.
            int savedPosition = mPosition;

            try
            {
                OpCodeStruct opcodeStruct = ReadOpCode();
                mPosition += opcodeStruct.OpCode.Size;

                switch (opcodeStruct.OperandSize)
                {
                    case 1: CurrentOperandInt8 = mCodeBuffer[mPosition]; break;
                    case 2: CurrentOperandInt16 =
                        BitConverter.ToInt16(mCodeBuffer, mPosition); break;
                    case 4: CurrentOperandInt32 =
                        BitConverter.ToInt32(mCodeBuffer, mPosition); break;
                    case 8: CurrentOperandInt64 =
                        BitConverter.ToInt64(mCodeBuffer, mPosition); break;
                }

                if (opcodeStruct.OperandSize != IndeterminateOperandSize)
                {
                    mPosition += opcodeStruct.OperandSize;
                }
                else
                {
                    int switchCount = BitConverter.ToInt32(mCodeBuffer, mPosition);
                    mPosition += (switchCount + 1) * 4;
                }

                CurrentOpCode = opcodeStruct.OpCode;
                CurrentOperandSize = opcodeStruct.OperandSize;
            }
            catch
            {
                // Restore object consistency and re-throw exception, as at that point
                // no other exception but @C OpCodeParserException may be normally caught.
                mPosition = savedPosition;
                throw;
            }
        }

        /**
         * @internal
         * Returns instruction opcode and advances current position. Please note, that
         * this method DOES NOT restore old position on failure -- it is done
         * in @c ReadOpCodeAndOperand instead.
         */
        private OpCodeStruct ReadOpCode()
        {
            OpCodeStruct result;

            short shortCode = mCodeBuffer[mPosition];
            if (mOpCodesHash.TryGetValue(shortCode, out result))
                return result;

            shortCode <<= 8;
            shortCode |= (short)mCodeBuffer[mPosition+1];
            if (mOpCodesHash.TryGetValue(shortCode, out result))
                return result;

            throw new OpCodeParserException("Unknown opcode found.");
        }

        /**
         * @internal
         * Creates a hash with all opcodes present in the current runtime environment.
         * For some reason there is no way to get the list of opcodes without using
         * reflection, so, we will just reflect @c System.Reflection.Emit.OpCodes
         * static class and iterate over its fields representing various opcodes.
         */
        private static OpCodesHash CreateOpCodesHash()
        {
            OpCodesHash result = new OpCodesHash();
            
            BindingFlags bindingFlags =
                BindingFlags.DeclaredOnly |
                BindingFlags.Public |
                BindingFlags.Static;
            foreach (FieldInfo fieldInfo in typeof(OpCodes).GetFields(bindingFlags))
            {
                if (fieldInfo.FieldType == typeof(OpCode))
                {
                    OpCode opcode = (OpCode)fieldInfo.GetValue(null);

                    // Thanks to Albahari for this check.
                    if (opcode.OpCodeType != OpCodeType.Nternal)
                    {
                        result.Add(opcode.Value, new OpCodeStruct() {
                            OpCode = opcode,
                            OperandSize = GetOperandSize(opcode),
                        });
                    }
                }
            }

            return result;
        }

        /**
         * @internal
         * Returns operand size or @c IndeterminateOperandSize for given @a opcode.
         */
        private static short GetOperandSize(OpCode opcode)
        {
            // Thanks to Albahari for this switch too.
            // TODO: May there be the cases when default section behaves in a wrong way?
            switch (opcode.OperandType)
            {
                case OperandType.InlineNone:
                    return 0;

                case OperandType.ShortInlineBrTarget:
                case OperandType.ShortInlineI:
                case OperandType.ShortInlineVar:
                    return 1;

                case OperandType.InlineVar:
                    return 2;

                case OperandType.InlineI8:
                case OperandType.InlineR:
                    return 8;

                case OperandType.InlineSwitch:
                    return IndeterminateOperandSize;

                default:
                    return 4;
            }
        }
    }
}
