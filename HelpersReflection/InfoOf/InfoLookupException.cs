﻿using System;
using System.Runtime.Serialization;

namespace Rvlm.CSharpHelpers.InfoOf
{
    /** 
     * Exception class for @c InfoOf.
     * This must be the only one exception class to be thrown from inside of @c InfoOf
     * class on errors.
     */
    [Serializable]
    public class InfoLookupException : Exception
    {
        public InfoLookupException() {}

        public InfoLookupException(string message)
            : base(message) {}

        public InfoLookupException(string message, Exception inner)
            : base(message, inner) {}

        protected InfoLookupException(SerializationInfo info, StreamingContext context)
            : base(info, context) {}
    }
}
