﻿using System;

namespace Rvlm.CSharpHelpers.InfoOf
{
    /** 
     * Exception class for @c OpCodeParser.
     * This must be the only one exception class to be thrown outside of @c OpCodeParser
     * on errors.
     * @see OpCodeParser
     */
    internal class OpCodeParserException : Exception
    {
        /** Creates new exception instance with given @a message. */
        public OpCodeParserException(string message)
            : base(message) { }

        /** Creates new exception instance with given @a message and @a innerException.*/
        public OpCodeParserException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}
