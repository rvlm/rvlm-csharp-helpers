﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace Rvlm.CSharpHelpers.InfoOf
{
    public static class InfoOf<T>
    {
        public static PropertyInfo GetPropertyInfo<TRet>(
            PropertyAccessLambda<T, TRet> lambda)
        {
            if (lambda == null)
                throw new InfoLookupException("Cannot process with null lambda.");

            try
            {
                var method = lambda.Method;
                return GetPropertyByGetMethod(
                    method.DeclaringType.Module.ResolveMethod(
                    FindSingleOpCodeToken(OpCodes.Callvirt,
                        method.GetMethodBody().GetILAsByteArray())));
            }
            catch (Exception e)
            {
                if (e is OpCodeParserException ||
                    e is ArgumentException)
                    throw new InfoLookupException(
                        "Cannot get extract property info from given lambda.", e);
                throw;
            }
        }

        public static EventInfo GetEventInfo(EventAccessLambda<T> lambda)
        {
            if (lambda == null)
                throw new InfoLookupException("Cannot process with null lambda.");

            try
            {
                var method = lambda.Method;
                return GetEventByAddMethod(
                    method.DeclaringType.Module.ResolveMethod(
                    FindSingleOpCodeToken(OpCodes.Callvirt,
                        method.GetMethodBody().GetILAsByteArray())));
            }
            catch (Exception e)
            {
                if (e is OpCodeParserException ||
                    e is ArgumentException)
                    throw new InfoLookupException(
                        "Cannot get extract event info from given lambda.", e);
                throw;
            }
        }

        /**
         * @internal
         * Parses CIL bytecode and looks up for given opcode.
         * Function takes CIL bytecode in @a ilcode parameter and parses it using
         * @c OpCodeParser looking for opcode given in @a opcode. If this opcode
         * is found and it the only one such opcode present, method returns its
         * operand (@c Int32 value). Otherwise an exception is raised.
         */
        private static Int32 FindSingleOpCodeToken(OpCode opcode, byte[] ilcode)
        {
            Debug.Assert(ilcode != null);

            // Valid token values don't include zero. For more information see:
            // http://stackoverflow.com/a/21905199/1447225
            Int32 token = 0;
            OpCodeParser parser = new OpCodeParser(ilcode);
            while (parser.NextOpCode())
            {
                if (parser.CurrentOpCode == opcode)
                {
                    if (token != 0)
                    {
                        throw new InfoLookupException(
                            string.Format("Multiple opcodes {0} found.", opcode));
                    }

                    token = parser.CurrentOperandInt32;
                }
            }

            if (token == 0)
            {
                throw new InfoLookupException(
                    string.Format("Cannot find opcode {0} in lambda bytecode.", opcode));
            }

            return token;
        }

        /**
         * @internal
         * Returns @c PropertyInfo for a property by its getter @a method or raises
         * exception if no such property found. Of course, the property must have
         * a getter for this to work.
         */
        private static PropertyInfo GetPropertyByGetMethod(MethodBase method)
        {
            Debug.Assert(method != null);
            if (!method.IsSpecialName || method.GetParameters().Length != 0)
                throw new InfoLookupException("Method is not a proper setter.");

            foreach (var prop in method.DeclaringType.GetProperties())
            {
                if (prop.GetGetMethod() == method)
                    return prop;
            }

            throw new InfoLookupException(
                "Cannot find property with given getter.");
        }

        /**
         * @internal
         * Returns @c EventInfo for by event adder @a method or raises exception if
         * no such property found. Of course, event must have adder method for this
         * to work.
         */
        private static EventInfo GetEventByAddMethod(MethodBase method)
        {
            Debug.Assert(method != null);
            if (!method.IsSpecialName || method.GetParameters().Length != 1)
                throw new InfoLookupException("Method is not a proper adder.");

            foreach (var evnt in method.DeclaringType.GetEvents())
            {
                if (evnt.GetAddMethod() == method)
                    return evnt;
            }

            throw new InfoLookupException(
                "Cannot find event with given adder.");
        }
    }
}
