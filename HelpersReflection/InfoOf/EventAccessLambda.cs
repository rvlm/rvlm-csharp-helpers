﻿using System;

namespace Rvlm.CSharpHelpers.InfoOf
{
    /**
     * Lambda expression which accesses some event.
     * The only one thing this lambda must do is to access an instance event of some
     * class in such a way that the event @c add method is the only one to be called.
     * Some examples:
     * @code
     *     EventAccessLambda<Control> lambda = c => c.TextChanged += null; // CORRECT
     *     EventAccessLambda<Control> lambda = c => c.Invalidate();        // INCORRECT
     *     EventAccessLambda<Control> lambda = c => Application.Exit();    // INCORRECT
     * @endcode
     * In the case an incorrect lambda expression is given to @c InfoOf,
     * nothing is guaranteed. Most likely, an expression will be thrown. Note that there
     * is no way for the compiler to check your lambda is correct in the above sense.
     * @see InfoOf.GetEventInfo
     * @see InfoLookupException
     */
    public delegate void EventAccessLambda<T>(T obj);
}
