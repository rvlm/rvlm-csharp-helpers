﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Rvlm.CSharpHelpers.InfoOf
{
    internal static class GlobalCache
    {
        public static ObjectCache<object, PropertyInfo> Properties =
            new ObjectCache<object, PropertyInfo>();

        public static ObjectCache<object, EventInfo> Events =
            new ObjectCache<object, EventInfo>();
    }

    public static class CacheInfoOf<T>
    {
        public static PropertyInfo GetPropertyInfo<TRet>(
            PropertyAccessLambda<T, TRet> lambda)
        {
            return GlobalCache.Properties.PassThrough(
                (object)lambda, () => InfoOf<T>.GetPropertyInfo<TRet>(lambda));
        }

        public static EventInfo GetEventInfo(EventAccessLambda<T> lambda)
        {
            return GlobalCache.Events.PassThrough(
                (object)lambda, () => InfoOf<T>.GetEventInfo(lambda));
        }
    }
}
