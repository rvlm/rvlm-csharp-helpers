﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Rvlm.CSharpHelpers.InfoOf;

namespace Rvlm.CSharpHelpers.PropertyNotification
{
    using ExtHandler = PropertyChangedExtEventHandler;
    using StdHandler = PropertyChangedEventHandler;

    /**
     * 
     */
    public static class PropertyNotificationHelper
    {
        /**
         * 
         */
        public static bool UpdatePropertyField<TObject, TValue>(this TObject obj, ref TValue member, TValue value)
            where TObject : INotifyPropertyChangedExt
        {
            if (object.Equals(member, value)) 
                return false;
            else 
            {
                member = value;
                return true;
            }
        }

        /**
         * 
         */
        public static void NotifyPropertyChange<T, TRet>(
            this T obj, PropertyAccessLambda<T, TRet> lambda)
            where T : INotifyPropertyChangedExt
        {
            var propInfo = CacheInfoOf<T>.GetPropertyInfo<TRet>(lambda);
            var extEvent = CacheInfoOf<T>.GetEventInfo(_ => _.PropertyChangedExt += null);
            var stdEvent = CacheInfoOf<T>.GetEventInfo(_ => _.PropertyChanged += null);
            TriggerEvent(obj, extEvent, new PropertyChangedExtEventArgs(obj, propInfo));
            TriggerEvent(obj, stdEvent, new PropertyChangedEventArgs(propInfo.Name));
        }

        /**
         * @internal
         * Raises  @a event with @a args on a given @a obj using reflection hack.
         * @see http://stackoverflow.com/a/3312601/1447225
         */
        private static void TriggerEvent(object obj, EventInfo info, EventArgs args)
        {
            var objType = obj.GetType();

            // Search the base type, which contains event field.
            FieldInfo fieldInfo = null;
            while (objType != null)
            {
                fieldInfo = objType.GetField(info.Name,
                    BindingFlags.Instance | BindingFlags.NonPublic);
                if (fieldInfo != null)
                    break;

                objType = objType.BaseType;
            }

            if (fieldInfo == null)
                throw new PropertyNotificationException("No internal field for event.");

            // Get event field value and hope it's nice.
            var fieldValue = fieldInfo.GetValue(obj);
            if (fieldValue == null)
                return;

            MulticastDelegate eventDelegate = fieldValue as MulticastDelegate;
            if (eventDelegate == null)
                throw new PropertyNotificationException("Not delegate to invoke.");

            // Invoke each delegate from invokation list.
            foreach (Delegate deleg in eventDelegate.GetInvocationList())
                deleg.Method.Invoke(deleg.Target, new object[] { obj, args });
        }
    }
}
