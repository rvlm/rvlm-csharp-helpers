﻿using System;
using System.ComponentModel;

namespace Rvlm.CSharpHelpers.PropertyNotification
{
    /**
     * Improved alternative to @c System.ComponentModel.INotifyPropertyChange.
     * Unlike the standard interface, uses property info objects to identify the property
     * been changed, thus allowing move consious code without having to put property
     * names into string constants.
     */
    public interface INotifyPropertyChangedExt : INotifyPropertyChanged
    {
        /**
         * Event raised on property change.
         * @see PropertyChangedExtEventHandler
         * @see PropertyChangedExtEventArgs
         */
        event PropertyChangedExtEventHandler PropertyChangedExt;
    }
}
