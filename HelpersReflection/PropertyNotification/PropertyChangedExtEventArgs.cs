﻿using System;
using System.Reflection;

namespace Rvlm.CSharpHelpers.PropertyNotification
{
    /**
     * Event handler delegate for @c INotifyPropertyChangeExt.
     * Defined mostly just to clarify code and can be replaced by equivalent definition
     * @c EventHandler<PropertyChangedExtEventArgs>.
     * @see PropertyChangedExtEventArgs
     */
    public delegate void PropertyChangedExtEventHandler(
        object sender, PropertyChangedExtEventArgs e);

    /**
     * Event handler arguments.
     * Unlike standard @c System.ComponentModel.PropertyChangedEventArgs holds not just
     * the name of related property, but complete reflection property info which allows
     * more convenient operations without magic strings with property names. 
     * @see PropertyChangedExtEventHandler
     */
    public sealed class PropertyChangedExtEventArgs : EventArgs
    {
        /**
         * Object which has property changed.
         * In general sence, it must not be the same object as the one passed by @a sender
         * argument to @c PropertyChangedExtEventHandler event handler. This property can
         * also be @c null in case of static property change notification.
         * @see Property
         */
        public Object Object { get; private set; }

        /**
         * Property info for changed property.
         * The property info (together with related @c Object) is enough for virtually
         * everything to do with changed property. Property info cannot be @c null.
         * @see Object
         */
        public PropertyInfo Property { get; private set; }

        /**
         * Создаёт экземпляр класса и устанавливает значение свойства @c Property.
         * В случае вызова конструктора со значением @c null будет выброшено исключение.
         * Constructs class instance and sets its properties.
         * This is the only one way to set @c Object and @c Property, as they're read
         * only. In case @a propertyInfo argument is @c null, and exception will be
         * thrown.
         * @throws ArgumentNullException
         * @see Property
         * @see Object
         */
        public PropertyChangedExtEventArgs(object obj, PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
                throw new ArgumentNullException("propertyInfo");

            Object = obj;
            Property = propertyInfo;
        }
    }
}
