﻿using System;
using System.Runtime.Serialization;

namespace Rvlm.CSharpHelpers.PropertyNotification
{
    [Serializable]
    public class PropertyNotificationException : Exception
    {
        public PropertyNotificationException() { }

        public PropertyNotificationException(string message)
            : base(message) { }

        public PropertyNotificationException(string message, Exception inner)
            : base(message, inner) { }

        protected PropertyNotificationException(
            SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
