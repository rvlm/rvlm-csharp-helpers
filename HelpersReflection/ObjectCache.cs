﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Rvlm.CSharpHelpers.InfoOf
{
    /**
     * @internal
     * Global cache for property info extraction.
     * As getting @c PropertyInfo objects from expression trees or bytecode may be quite
     * slow, it is considered as a good idea to cache it, which is this class was written
     * for. It uses @ Rvlm.CSharpHelpers library for actual property info extraction.
     * 
     * This class methods are thread-safe, but (currently) may cause delays when cache
     * updates frequently.
     */
    public class ObjectCache<TKey, TVal>
    {
        static private Dictionary<TKey, TVal> mCache1 = new Dictionary<TKey, TVal>();

        static private Dictionary<TKey, TVal> mCache2 = new Dictionary<TKey, TVal>();

        static private object mInsertionSync = new object();

        public delegate TVal LazyGetterDelegate();

        /**
          * Returns property info for lambda with automatic global caching.
          * If the corresponding @c PropertyInfo object is already present in cache,
          * returns cached entry without actual exctraction. Otherwise, extracts property
          * info from @a lambda, caches it, and returns.
          */
        public TVal PassThrough(TKey key, LazyGetterDelegate lookup)
        {
            TVal result;
            if (!mCache1.TryGetValue(key, out result))
            {
                result = lookup();

                // No @c Interlocked class or explicit memory barrier usage is needed
                // because reference assignments are guaranteed to be atomic and 'lock'
                // keyword implies memory barriers on both entering and leaving.
                // @see http://stackoverflow.com/questions/2192124
                // @see http://stackoverflow.com/questions/2844452
                lock (mInsertionSync)
                {
                    var cache1 = mCache1;
                    var cache2 = mCache2;
                    cache2.Add(key, result);
                    mCache1 = cache2;
                    mCache2 = cache1;
                    cache1.Add(key, result);
                }
            }

            return result;
        }
    }
}
