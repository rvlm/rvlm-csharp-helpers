﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using Rvlm.CSharpHelpers.WindowsForms;
using NUnit.Framework;

namespace Rvlm.UnitTests.CSharpHelpers.WindowsForms
{
    [TestFixture]
    public class TemplateControlHelperTest
    {
        private Label templateLabel = new Label(){
            Name      = "TemplateLabel",
            Text      = "-- Text --",
            BackColor = Color.Black,
            ForeColor = Color.White,
            Font      = new Font(FontFamily.GenericMonospace, 10), 
            Padding   = new Padding(8),
        };

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullArguments1()
        {
            TemplateControlHelper.CopyTemplate((Label)null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullArguments2()
        {
            TemplateControlHelper.ApplyTemplateSettings((Label)null, null);
        }

        [Test]
        public void CopyTemplate()
        {
            Label targetLabel = TemplateControlHelper.CopyTemplate(templateLabel);
            CompareLabels(targetLabel, templateLabel);
        }

        [Test]
        public void ApplyTemplateSettings()
        {
            Label targetLabel = new Label();
            TemplateControlHelper.ApplyTemplateSettings(targetLabel, templateLabel);
            CompareLabels(targetLabel, templateLabel);
        }

        private void CompareLabels(Label targetLabel, Label templateLabel)
        {
            Assert.AreEqual(templateLabel.Text,      targetLabel.Text);
            Assert.AreEqual(templateLabel.BackColor, targetLabel.BackColor);
            Assert.AreEqual(templateLabel.ForeColor, targetLabel.ForeColor);
            Assert.AreEqual(templateLabel.Font,      targetLabel.Font);
            Assert.AreEqual(templateLabel.Padding,   targetLabel.Padding);

            // This one mustn't be copied.
            Assert.AreNotEqual(templateLabel.Name, targetLabel.Name);
        }
    }
}
