@echo off
set SOLUTION=Rvlm.CSharpHelpers.sln
set CONFIGURATION=Release
set OUTPUTDIR=build

set VARIANT=net20
set FRAMEWORK=v2.0
set CONSTANTS=TARGET_FRAMEWORK_VERSION_20
call :build

set VARIANT=net35
set FRAMEWORK=v3.5
set CONSTANTS=TARGET_FRAMEWORK_VERSION_35
call :build

call :pack

pause
exit

:build
echo *** Building solution variant %VARIANT% ...
msbuild %SOLUTION%                         ^
    /verbosity:quiet /nologo               ^
    /t:Rebuild                             ^
    /p:Configuration=%CONFIGURATION%       ^
    /p:TargetFrameworkVersion=%FRAMEWORK%  ^
    /p:DefineConstants=%CONSTANTS%         ^
    /p:OutputPath=..\%OUTPUTDIR%\%VARIANT%
goto :EOF

:pack
echo *** Packing solution into NuGet package ...
nuget pack %SOLUTION%.nuspec -OutputDirectory %OUTPUTDIR%
goto :EOF
