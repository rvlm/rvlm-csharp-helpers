﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rvlm.CSharpHelpers.InfoOf;
using NUnit.Framework;

namespace Rvlm.UnitTests.CSharpHelpers.InfoOf
{
    using Assert = NUnit.Framework.Assert;

    [TestFixture]
    public class InfoOfTest
    {
        public class BaseClass
        {
            public event EventHandler Event;

            public virtual string Property { get; set; }
            public bool ReadOnlyProperty { get { return false; } }
            public bool Method() { return false; }
            public virtual bool VirtualMethod() { return false; }
        }

        public class DerivedClass : BaseClass
        {
            public override string Property { get; set; }
            public override bool VirtualMethod() { return true; }
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongLambda1()
        {
            InfoOf<BaseClass>.GetPropertyInfo(b => b);
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongLambda2()
        {
            InfoOf<BaseClass>.GetPropertyInfo(b => b.Method());
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongLambda3()
        {
            InfoOf<BaseClass>.GetPropertyInfo(b => b.VirtualMethod());
        }

        [Test]
        public void StringProperties()
        {
            Assert.AreEqual(
                typeof(string).GetProperty("Length"),
                InfoOf<string>.GetPropertyInfo(s => s.Length));
        }

        [Test]
        public void ControlProperties()
        {
            Assert.AreEqual(
                typeof(Control).GetProperty("ForeColor"),
                InfoOf<Control>.GetPropertyInfo(c => c.ForeColor));
            Assert.AreEqual(
                typeof(Control).GetProperty("Text"),
                InfoOf<Control>.GetPropertyInfo(c => c.Text));
            Assert.AreEqual(
                typeof(Control).GetProperty("Handle"),
                InfoOf<Control>.GetPropertyInfo(c => c.Handle));
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongEventLambda1()
        {
            InfoOf<BaseClass>.GetEventInfo(b => { ;});
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongEventLambda2()
        {
            InfoOf<BaseClass>.GetEventInfo(b => { b.Method(); });
        }

        [Test]
        [ExpectedException(typeof(InfoLookupException))]
        public void WrongEventLambda3()
        {
            InfoOf<BaseClass>.GetEventInfo(b => { 
                b.Method(); b.VirtualMethod(); });
        }

        public void BaseEvent()
        {
            Assert.AreEqual(
                typeof(BaseClass).GetEvent("Event"),
                InfoOf<BaseClass>.GetEventInfo(c => c.Event += null));
        }

        public void DerivedEvents()
        {
            Assert.AreEqual(
                typeof(DerivedClass).GetEvent("Event"),
                InfoOf<DerivedClass>.GetEventInfo(c => c.Event += null));
        }

        [Test]
        public void ControlEvents()
        {
            Assert.AreEqual(
                typeof(Control).GetEvent("TextChanged"),
                InfoOf<Control>.GetEventInfo(c => c.TextChanged += null));
            Assert.AreEqual(
                typeof(Control).GetEvent("Validating"),
                InfoOf<Control>.GetEventInfo(c => c.Validating += null));
        }
    }
}
