﻿using System;
using System.ComponentModel;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Rvlm.UnitTests.CSharpHelpers.InfoOf.PropertyNotification
{
    [TestFixture]
    public class PropertyNotificationHelperTest
    {
        [Test]
        public void TestBase()
        {
            BaseClass obj = new BaseClass();
            string objPropNameStd = null;
            string objPropNameExt = null;
            obj.PropertyChanged    += (_, e) => objPropNameStd = e.PropertyName;
            obj.PropertyChangedExt += (_, e) => objPropNameExt = e.Property.Name;
            obj.Property = 1;
            Assert.AreEqual("Property", objPropNameStd);
            Assert.AreEqual("Property", objPropNameExt);

            objPropNameStd = null;
            objPropNameExt = null;
            obj.Property = 1;
            Assert.AreEqual(null, objPropNameStd);
            Assert.AreEqual(null, objPropNameExt);
        }

        [Test]
        public void TestDerived()
        {
            DerivedClass obj = new DerivedClass();
            string objPropNameStd = null;
            string objPropNameExt = null;
            obj.PropertyChanged    += (_, e) => objPropNameStd = e.PropertyName;
            obj.PropertyChangedExt += (_, e) => objPropNameExt = e.Property.Name;
            obj.Property = 1;
            Assert.AreEqual("Property", objPropNameStd);
            Assert.AreEqual("Property", objPropNameExt);

            objPropNameStd = null;
            objPropNameExt = null;
            obj.Property = 1;
            Assert.AreEqual(null, objPropNameStd);
            Assert.AreEqual(null, objPropNameExt);
        }

        [Test]
        public void TestDerived2()
        {
            DerivedClass obj = new DerivedClass();
            string objPropNameStd = null;
            string objPropNameExt = null;
            obj.PropertyChanged    += (_, e) => objPropNameStd = e.PropertyName;
            obj.PropertyChangedExt += (_, e) => objPropNameExt = e.Property.Name;
            obj.Property2 = 1;
            Assert.AreEqual("Property2", objPropNameStd);
            Assert.AreEqual("Property2", objPropNameExt);

            objPropNameStd = null;
            objPropNameExt = null;
            obj.Property2 = 1;
            Assert.AreEqual(null, objPropNameStd);
            Assert.AreEqual(null, objPropNameExt);
        }
    }
}
