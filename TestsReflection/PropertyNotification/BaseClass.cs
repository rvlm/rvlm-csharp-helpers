﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Rvlm.CSharpHelpers.PropertyNotification;

namespace Rvlm.UnitTests.CSharpHelpers.InfoOf.PropertyNotification
{
    class BaseClass : INotifyPropertyChangedExt
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangedExtEventHandler PropertyChangedExt;

        private int mProperty;
        public int Property {
            get { return mProperty; }
            set { if (this.UpdatePropertyField(ref mProperty, value))
                      this.NotifyPropertyChange(_ => _.Property);}
        }
    }
}
