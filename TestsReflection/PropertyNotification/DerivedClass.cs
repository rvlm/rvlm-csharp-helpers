﻿using System;
using System.Collections.Generic;
using Rvlm.CSharpHelpers.PropertyNotification;

namespace Rvlm.UnitTests.CSharpHelpers.InfoOf.PropertyNotification
{
    class DerivedClass : BaseClass
    {
        private int mProperty2;
        public int Property2 {
            get { return mProperty2; }
            set { if (this.UpdatePropertyField(ref mProperty2, value))
                      this.NotifyPropertyChange(_ => _.Property2);}
        }
    }
}
