﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Windows.Forms;

namespace Rvlm.CSharpHelpers.WindowsForms
{
    /**
     * Helper methods for creating copies of Windows.Forms controls.
     * It is useful then you have to create several simularily looking controls
     * in runtime and do not want to set up all properties manually. The methods of this
     * class are not intended to be a true @c Clone() methods (controls do not implement
     * ICloneable as you may know), they just copy the values of properties using
     * reflection. Only designer visible attributes are taken into account, so, make sure
     * you have at least @c DefaultValueAtrribute set on your own controls' properties.
     * @see CopyTemplate
     * @see ApplyTemplateSettings
     */
    public static class TemplateControlHelper
    {
        /**
         * @internal
         * Long list of attributes whose presence indicates that the property is intended
         * for use by Visual Studio designer.
         * The list is based on http://msdn.microsoft.com/en-us/library/ms171726.aspx.
         * Here is used @c Dictionary container instead of @c HashSet, because the latter
         * one was introduced in .NET 3.5.
         */
        private static Dictionary<Type, bool> msDesignerAttributes =
            new Dictionary<Type, bool>() {
                { typeof(AmbientValueAttribute),                    true },
                { typeof(BindableAttribute),                        true },
                { typeof(BrowsableAttribute),                       true },
                { typeof(CategoryAttribute),                        true },
                { typeof(DefaultValueAttribute),                    true },
                { typeof(DescriptionAttribute),                     true },
                { typeof(DesignerSerializationVisibilityAttribute), true },
                { typeof(DisplayNameAttribute),                     true },
                { typeof(EditorAttribute),                          true },
                { typeof(EditorBrowsableAttribute),                 true },
                { typeof(HelpKeywordAttribute),                     true },
                { typeof(LocalizableAttribute),                     true },
                { typeof(ParenthesizePropertyNameAttribute),        true },
                { typeof(PasswordPropertyTextAttribute),            true },
                { typeof(ReadOnlyAttribute),                        true },
                { typeof(RefreshPropertiesAttribute),               true },
                { typeof(TypeConverterAttribute),                   true },
            };

        /**
         * Created a new control and applies @a template properties to it.
         * Please note that, first, the default constructor is used to create new control,
         * and, second, only designer visible properties are taken into account.
         * @see ApplyTemplateSettings
         */
        public static TControl CopyTemplate<TControl>(TControl templateControl)
            where TControl : Control, new()
        {
            if (templateControl == null)
                throw new ArgumentNullException("templateControl");

            TControl result = new TControl();
            ApplyTemplateSettings(result, templateControl);
            return result;
        }

        /**
         * Applies properties from @a templateControl to @a targetControl.
         * Please note that only designer visible properties are taken into account.
         * @see CopyTemplate
         */
        public static void ApplyTemplateSettings<TControl>(
            TControl targetControl,
            TControl templateControl)
            where TControl : Control
        {
            if (targetControl == null)
                throw new ArgumentNullException("targetControl");

            if (templateControl == null)
                throw new ArgumentNullException("templateControl");

            // Those properties don't have any designer specific attributes.
            targetControl.AccessibleRole = templateControl.AccessibleRole;
            targetControl.BackColor = templateControl.BackColor;
            targetControl.ForeColor = templateControl.ForeColor;

            foreach (PropertyInfo propInfo in typeof(TControl).GetProperties(
                BindingFlags.Public | BindingFlags.Instance))
            {
                if (!propInfo.CanWrite)
                    continue;

                // FIXME: A little bit hacky but should be okay in most cases.
                if (propInfo.Name == "Name")
                    continue;

                bool foundAny;
                bool foundDefaultValue;
                object defaultValue;

                InvestigateAttributes(propInfo,
                    out foundAny,
                    out foundDefaultValue,
                    out defaultValue);

                if (foundAny)
                {
                    object propValue = propInfo.GetValue(templateControl, null);
                    if (!foundDefaultValue || !object.Equals(propValue, defaultValue))
                        propInfo.SetValue(targetControl, propValue, null);
                }
            }
        }

        /**
         * @internal
         * Iterates over property's custom attributes and checks if it is a designer
         * specific property, and returns designer's default value for property if it was
         * given. Returns @c true in @a foundAny when any designer specific properties
         * found, @c true in @a foundDefaultValue when @c DefaultValueAttribute found
         * with its value in @a defaultValue.
         */
        private static void InvestigateAttributes(
            PropertyInfo propInfo,
            out bool foundAny,
            out bool foundDefaultValue,
            out object defaultValue)
        {
            foundAny = false;
            foundDefaultValue = false;
            defaultValue = null;

            foreach (object attr in propInfo.GetCustomAttributes(true))
            {
                if (foundAny && foundDefaultValue)
                    break;

                Type attrType = attr.GetType();

                bool dummy;
                foundAny |= msDesignerAttributes.TryGetValue(attrType, out dummy);

                if (!foundDefaultValue && attrType == typeof(DefaultValueAttribute))
                {
                    defaultValue = ((DefaultValueAttribute)attr).Value;
                    foundDefaultValue = true;
                }
            }
        }
    }
}
