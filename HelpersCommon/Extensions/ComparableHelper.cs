﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    public static class ComparableHelper
    {
        public static T RestrictToRange<T>(this T val, T left, T right)
            where T : IComparable<T>
        {
            if (val.CompareTo(left) <= 0)
                return left;

            if (val.CompareTo(right) >= 0)
                return right;

            return val;
        }
    }
}
