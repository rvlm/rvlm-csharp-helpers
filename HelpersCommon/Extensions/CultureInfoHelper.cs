﻿using System;
using System.Globalization;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * Helper class with extension methods for @c System.Globalization.CultureInfo.
     * Currently it introduces only two methods: @c IsAncestorOf and @c IsDescendantOf,
     * for dealing with cultures hierarchy. These methods are linked with the following
     * assertion for any non-null @a child and @a parent cultures:
     * @code
     *     parent.IsAncestorOf(child) == child.IsDescendantOf(parent)
     * @endcode
     */
    public static class CultureInfoHelper
    {
        /**
         * Finds out if @a child culture is a ancestor of @a parent culture, direct or
         * indirect.
         * For example, neutral culture is ancestor for both 'ru' and 'ru-RU', but the
         * first one is direct child, while the second one is indirect. Argument @a child
         * must not be @c null or @c ArgumentNullException will be thrown.
         * @see IsDescendantOf
         */
        public static bool IsAncestorOf(this CultureInfo parent, CultureInfo child)
        {
            if (parent == null) throw new ArgumentNullException("parent");
            if (child == null) throw new ArgumentNullException("child");

            // Despite the code looks quite simple, it was refactored many times before
            // it got to work. Please note 'Equals' comparison instead of '=='.
            CultureInfo current = child;
            CultureInfo invariant = CultureInfo.InvariantCulture;
            while (!current.Equals(parent) && !current.Equals(invariant))
                current = current.Parent;

            return !current.Equals(invariant);
        }

        /**
         * Finds out if @a child culture is a descendant of @a parent culture, direct or
         * indirect.
         * For example, both 'ru' and 'ru-RU' are descendants of neutral culture, but the
         * first one is direct child, while the second one is indirect. Argument @a parent
         * must not be @c null or @c ArgumentNullException will be thrown.
         * @see IsAncestorOf
         */
        public static bool IsDescendantOf(this CultureInfo child, CultureInfo parent)
        {
            if (parent == null) throw new ArgumentNullException("parent");
            if (child == null) throw new ArgumentNullException("child");
            return parent.IsAncestorOf(child);
        }
    }
}
