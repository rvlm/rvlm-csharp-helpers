﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    public static class StringHelper
    {
        public static string Fmt(this string fmt, object arg0)
        {
            return string.Format(fmt, arg0);
        }

        public static string Fmt(this string fmt, object arg0, object arg1)
        {
            return string.Format(fmt, arg0, arg1);
        }

        public static string Fmt(
            this string fmt, object arg0, object arg1, object arg2)
        {
            return string.Format(fmt, arg0, arg1, arg2);
        }

        public static string Fmt(this string fmt, params object[] args)
        {
            return string.Format(fmt, args);
        }
        //! @}

        /**
         */
        //! @{
        public static KeyValuePair<string, string> Fmt(
            this KeyValuePair<string, string> fmt, object arg0)
        {
            return new KeyValuePair<string,string>(
                string.Format(fmt.Key, arg0),
                string.Format(fmt.Value, arg0));
        }

        public static KeyValuePair<string, string> Fmt(
            this KeyValuePair<string, string> fmt, object arg0, object arg1)
        {
            return new KeyValuePair<string, string>(
                string.Format(fmt.Key, arg0, arg1),
                string.Format(fmt.Value, arg0, arg1));
        }

        public static KeyValuePair<string, string> Fmt(
            this KeyValuePair<string, string> fmt, object arg0, object arg1, object arg2)
        {
            return new KeyValuePair<string, string>(
                string.Format(fmt.Key, arg0, arg1, arg2),
                string.Format(fmt.Value, arg0, arg1, arg2));
        }

        public static KeyValuePair<string, string> Fmt(
            this KeyValuePair<string, string> fmt, params object[] args)
        {
            return new KeyValuePair<string, string>(
                string.Format(fmt.Key, args),
                string.Format(fmt.Value, args));
        }
        //! @}
        public static bool StartsWith(this string str, char subchar)
        {
            return str.Length > 0 && str[0] == subchar;
        }

        public static bool EndsWith(this string str, char subchar)
        {
            return str.Length > 0 && str[str.Length-1] == subchar;
        }

        public static bool Contains(this string str, string substring, int offset)
        {
            if (substring == null)
                throw new ArgumentNullException("Substring is null.");

            if (offset < 0)
                throw new ArgumentOutOfRangeException("Negative offset.");

            if (str == string.Empty)
                return true;

            if (str.Length < substring.Length + offset)
                return false;

            bool matched = true;
            for (int i=offset, j=0; j<substring.Length; i++,j++)
            {
                if (str[i] != substring[j])
                {
                    matched = false;
                    break;
                }
            }

            return matched;
        }

        public static string[] SplitKeepEmptyEntries(
            this string text, params char[] delimiters)
        {
            return text.Split(delimiters, StringSplitOptions.None);
        }

        public static string[] SplitKeepEmptyEntries(
            this string text, params string[] delimiters)
        {
            return text.Split(delimiters, StringSplitOptions.None);
        }

        public static string[] SplitOmitEmptyEntries(
            this string text, params char[] delimiters)
        {
            return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SplitOmitEmptyEntries(
            this string text, params string[] delimiters)
        {
            return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string ReplaceMultiple(
            this string str, IList<string> patterns, IList<string> replaces)
        {
            if (patterns == null)
                throw new ArgumentNullException("Patterns array is null.");

            if (replaces == null)
                throw new ArgumentNullException("Replaces array is null.");

            if (patterns.Count != replaces.Count)
                throw new ArgumentException("Number of patterns and replaces differs.");

            foreach (string pattern in patterns)
            {
                if (string.IsNullOrEmpty(pattern))
                    throw new ArgumentException("Null or empty value in patterns.");
            }

            foreach (string replace in replaces)
            {
                if (replace == null)
                    throw new ArgumentException("Null value in replaces.");
            }

            return ReplaceMultipleInternal(str,
                EnumerableHelper.Interleave(patterns, replaces));
        }

        public static string ReplaceMultiple(
            this string str, params string[] patternsAndReplaces)
        {
            if (patternsAndReplaces == null)
                return str;

            if (patternsAndReplaces.Length % 2 != 0)
                throw new ArgumentException("Odd number of patterns/replacements.");

            bool even = true;
            foreach (string patternOrReplace in patternsAndReplaces)
            {
                if (even && string.IsNullOrEmpty(patternOrReplace))
                    throw new ArgumentException("Null or empty value in patterns.");

                if (!even && patternOrReplace == null)
                    throw new ArgumentException("Null value in replaces.");

                even = !even;
            }

            return ReplaceMultipleInternal(str, patternsAndReplaces);
        }

        private static string ReplaceMultipleInternal(
            string str, IEnumerable<string> patternsAndReplaces)
        {
            StringBuilder sbuilder = new StringBuilder();
            int i = 0;
            while (i < str.Length)
            {
                string pattern;
                string replace;
                bool wasReplaced = false;

                IEnumerator<string> enumerator = patternsAndReplaces.GetEnumerator();
                while (enumerator.MoveNext() && (pattern = enumerator.Current) != null &&
                       enumerator.MoveNext() && (replace = enumerator.Current) != null) 
                {
                    if (str.Contains(pattern, i))
                    {
                        sbuilder.Append(replace);
                        i += pattern.Length;
                        wasReplaced = true;
                        break;
                    }
                }

                if (!wasReplaced)
                {
                    sbuilder.Append(str[i]);
                    i++;
                }
            }

            return sbuilder.ToString();
        }
    }
}
