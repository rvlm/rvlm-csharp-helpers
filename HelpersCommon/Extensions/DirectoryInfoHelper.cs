﻿using System;
using System.IO;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * Helper class with extensions methods for @c System.IO.DirectoryInfo.
     * Because sometimes system classes lack very obvious features.
     */
    public static class DirectoryInfoHelper
    {
        /**
         * Returns information for subdirectory with given @a name which is placed in
         * directory with given @a directoryInfo.
         * @see GetFile
         */
        public static DirectoryInfo GetDirectory(
            this DirectoryInfo directoryInfo, string name)
        {
            return new DirectoryInfo(Path.Combine(directoryInfo.FullName, name));
        }

        /**
         * Returns information for file with given @a name which is placed in directory
         * with given @a directoryInfo.
         * @see GetDirectory
         */
        public static FileInfo GetFile(
            this DirectoryInfo directoryInfo, string name)
        {
            return new FileInfo(Path.Combine(directoryInfo.FullName, name));
        }
    }
}
