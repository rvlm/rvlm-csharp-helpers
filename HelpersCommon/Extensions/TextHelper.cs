﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * Helper class text manipulation.
     */
    public static class TextHelper
    {
        private static readonly char[] HexDigits = new char[] {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        private static readonly Dictionary<char,int> HexDigitsValues 
            = new Dictionary<char,int>{
            {'0', 0},  {'1', 1},  {'2', 2},  {'3', 3},
            {'4', 4},  {'5', 5},  {'6', 6},  {'7', 7},
            {'8', 8},  {'9', 9},  {'a', 10}, {'b', 11},
            {'c', 12}, {'d', 13}, {'e', 14}, {'f', 15},
            {'A', 10}, {'B', 11}, {'C', 12}, {'D', 13},
            {'E', 14}, {'F', 15}
        };

        /**
         * Encodes byte sequence into string of hex dump.
         * Every byte is represented by two digits, in lower case. This method is a
         * performance overload for better memory usage when the length of result
         * string can be easily precalculated.
         * @see EncodeToHex(IEnumerable<byte>)
         */
        public static string EncodeToHex(IList<byte> sequence)
        {
            StringBuilder sbuilder = new StringBuilder(sequence.Count*2);
            foreach (byte octet in sequence)
            {
                sbuilder.Append(HexDigits[octet >> 4]);
                sbuilder.Append(HexDigits[octet & 0x0f]);
            }

            return sbuilder.ToString();
        }

        /**
         * Encodes byte sequence into string of hex dump.
         * Every byte is represented by two digits, in lower case. This method has
         * slightly worse memory performance, because the length of result string can
         * not be easily precalculated.
         * @see EncodeToHex(IList<byte>)
         */
        public static string EncodeToHex(IEnumerable<byte> sequence)
        {
            StringBuilder sbuilder = new StringBuilder();
            foreach (byte octet in sequence)
            {
                sbuilder.Append(HexDigits[octet >> 4]);
                sbuilder.Append(HexDigits[octet & 0x0f]);
            }

            return sbuilder.ToString();
        }

        /**
         * Decodes hex dump string into byte array.
         * @see EncodeToHex(IList<byte>)
         * @see EncodeToHex(IEnumerable<byte>)
         */
        public static byte[] DecodeFromHex(string hex)
        {
            if (hex == null)
                throw new ArgumentNullException("hex");

            if (hex.Length % 2 != 0)
                throw new ArgumentException("Odd number of hexadecimal digits in dump.");

            byte[] result = new byte[hex.Length/2];

            int offset = 0;
            IEnumerator<char> enumerator = hex.GetEnumerator();
            while (enumerator.MoveNext())
            {
                char first = enumerator.Current;

                enumerator.MoveNext();
                char second = enumerator.Current;

                int firstVal;
                if (!HexDigitsValues.TryGetValue(first, out firstVal))
                    return null;

                int secondVal;
                if (!HexDigitsValues.TryGetValue(second, out secondVal))
                    return null;

                result[offset] = (byte)((firstVal << 4) + secondVal);
                offset++;
            }

            return result;
        }
    }
}
