﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * 
     */
    public static class ListHelper
    {
        /**
         * Extended binary search function with mapping and comparison.
         */
        public static int BinarySearchBy<T, TKey>(
            this IList<T> list, int index, int count,
            Func<T, TKey> map, TKey key, Comparison<TKey> comparison)
        {
            int left = index;
            int right = index + count - 1;
            while (left <= right)
            {
                int mid = (right + left) / 2;
                int res = comparison(map(list[mid]), key);
                if (res == 0)
                    return mid;
                if (res < 0)
                    left = mid + 1;
                else 
                    right = mid - 1;
            }

            return ~left;
        }

        /**
         * Convenience overload for @c BinarySearchBy which searches whole list.
         * @see BinarySearchBy(IList<T>, int, int, Func<T,TKey>, TKey, Comparison<T>)
         */
        public static int BinarySearchBy<T, TKey>(
            this IList<T> list, Func<T, TKey> map, TKey key, Comparison<TKey> comparison)
        {
            return ListHelper.BinarySearchBy(list, 0, list.Count, map, key, comparison);
        }

        /**
         * Convenience overload for @c BinarySearchBy which searches whole list with
         * default comparison.
         * @see BinarySearchBy(IList<T>, int, int, Func<T,TKey>, TKey, Comparison<T>)
         */
        public static int BinarySearchBy<T, TKey>(
            this IList<T> list, Func<T, TKey> map, TKey key)
            where TKey : IComparable<TKey>
        {
            return ListHelper.BinarySearchBy(list, map, key, (a,b) => a.CompareTo(b));
        }

        /**
         * Binary search with @c Comparision<T> instead of @c IComparer<T>.
         * This method follows syntax and semantics of @c List<T>.BinarySearch method,
         * but works with delegates instead of comparators. Please, see MSDN for complete
         * explanation.
         * @see http://msdn.microsoft.com/ru-ru/library/w4e7fxsh%28v=vs.110%29.aspx
         * @see http://en.wikipedia.org/wiki/Binary_search_algorithm
         */
        public static int BinarySearch<T>(
            this IList<T> list, int index, int count, T item, Comparison<T> comparison)
        {
            return ListHelper.BinarySearchBy(list, index, count, _=>_, item, comparison);
        }

        /**
         * Convenience overload for @c BinarySearch which searches whole list.
         * @see BinarySearch(IList<T>, int, int, T, Comparison<T>)
         */
        public static int BinarySearch<T>(
            this IList<T> list, T item, Comparison<T> comparison)
        {
            return ListHelper.BinarySearch(list, 0, list.Count, item, comparison);
        }

        /**
         * Add item to sorted list using binary search.
         */
        public static void BinarySearchAdd<T>(
            this IList<T> list, T item, Comparison<T> comparison)
        {
            int idx = ListHelper.BinarySearch(list, item, comparison);
            if (idx < 0)
                idx = ~idx;

            list.Insert(idx, item);
        }

        /**
         * Convenience overload for @c BinarySearchAdd with default comparison.
         * @see BinarySearchAdd(IList<T>, T, Comparison<T>)
         */
        public static void BinarySearchAdd<T>(this IList<T> list, T item)
            where T : IComparable<T>
        {
            ListHelper.BinarySearchAdd(list, item, (a,b) => a.CompareTo(b));
        }

        /**
         * Remove item from sorted list using binary search.
         * Returns @c true if item was found and removed, overwise returns @c false.
         */
        public static bool BinarySearchRemove<T>(
            this List<T> list, T item, Comparison<T> comparison)
        {
            int idx = ListHelper.BinarySearch(list, item, comparison);
            if (idx >= 0)
                return list.Remove(item);

            return false;
        }

        /**
         * Convenience overload for @c BinarySearchRemove with default comparison.
         * @see BinarySearchRemove(IList<T>, T, Comparison<T>)
         */
        public static bool BinarySearchRemove<T>(this List<T> list, T item)
            where T : IComparable<T>
        {
            return ListHelper.BinarySearchRemove(list, item, (a,b) => a.CompareTo(b));
        }
    }
}
