﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * Helper class for @c System.Collection.Generic.IEnumerable<T> sequences.
     */
    public static class EnumerableHelper
    {
        /**
         * Adds all items from @a source to @a dest.
         * Helper method for easy reusing existing list variables instead of recreation.
         */
        public static void AddRange<T>(this IList<T> dest, IEnumerable<T> source)
        {
            foreach (T item in source)
                dest.Add(item);
        }

        /**
         * Sets @a dest's items to those from all items from @a source.
         * Helper method for easy reusing existing list variables instead of recreation.
         */
        public static void FromRange<T>(this IList<T> dest, IEnumerable<T> source)
        {
            dest.Clear();
            AddRange(dest, source);
        }

        /**
         * Runs given @a action on every item in @a sequence.
         * It's arguing that using of this method may be a bad idea. Read the following:
         * http://blogs.msdn.com/b/ericlippert/archive/2009/05/18/foreach-vs-foreach.aspx.
         */
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (T obj in collection)
                action(obj);
        }

        /**
         * Runs given @a action on every item in @a sequence.
         * Another version of @c ForEach which also pass @a item's index as @a actions's
         * second parameter.
         * @see ForEach(IEnumerable<T>, Action<T>)
         */
        public static void ForEach<T>(
            this IEnumerable<T> collection, Action<T,int> action)
        {
            int idx = 0;
            foreach (T obj in collection)
            {
                action(obj, idx);
                ++idx;
            }
        }

        /**
         * Stringify @a sequence items and join them into single string with
         * given @a separator.
         */
        public static string StringJoin<T>(
            this IEnumerable<T> sequence, string separator)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in sequence)
            {
                builder.Append(item.ToString());
                builder.Append(separator);
            }

            return builder.Length > 0
                ? builder.ToString(0, builder.Length - separator.Length)
                : builder.ToString();
        }

        /**
         * Returns a copy of @a sequence sorted in ascending order.
         * This method is a helper created to eliminate the following equivalent code:
         * @code
         *     sequence.OrderBy(v => v)
         * @endcode
         * @see SortedDescending
         */
        public static IEnumerable<T> Sorted<T>(this IEnumerable<T> sequence)
        {
            return sequence.OrderBy(v => v);
        }

        /**
         * Returns a copy of @a sequence sorted in descending order.
         * This method is a helper created to eliminate the following equivalent code:
         * @code
         *     sequence.OrderByDescending(v => v)
         * @endcode
         * @see Sorted
         */
        public static IEnumerable<T> SortedDescending<T>(this IEnumerable<T> sequence)
        {
            return sequence.OrderByDescending(v => v);
        }

        /**
         * Enumerates @a sequence for index of first item satisfacting @a predicate.
         * If such an item is found, its index is returned (indexing starts with zero).
         * Otherwise @c -1 is returned.
         */
        public static int IndexOf<T>(this IEnumerable<T> sequence, Func<T, bool> predicate)
        {
            int index = 0;
            foreach (var item in sequence)
            {
                if (predicate(item))
                    return index;

                ++index;
            }

            return -1;
        }

        /**
         * Enumerates from @a sequence only items with odd indexes.
         * Indexing starts with zero.
         * @see Evens
         */
        public static IEnumerable<T> Odds<T>(this IEnumerable<T> sequence)
        {
            return sequence.Where((v, idx) => idx % 2 != 0);
        }

        /**
         * Enumerates from @a sequence only items with even indexes.
         * Indexing starts with zero.
         * @see Odds
         */
        public static IEnumerable<T> Evens<T>(this IEnumerable<T> sequence)
        {
            return sequence.Where((v, idx) => idx % 2 == 0);
        }

        /**
         * Interleaves two enumerable sequences.
         * Both @a first and @a second don't have to be finite, but they must be not
         * @c null. Operation of this method is best described with an example:
         * @code
         *     Interlace("abcde", "fgh") => 'a', 'f', 'b', 'g', 'c', 'h', 'd', 'e'
         * @endcode
         */
        public static IEnumerable<T> Interleave<T>(
            IEnumerable<T> first, IEnumerable<T> second)
        {
            var firstEnumerator = first.GetEnumerator();
            var secondEnumerator = second.GetEnumerator();

            bool empty = true;
            do
            {
                if (firstEnumerator.MoveNext())
                {
                    empty = false;
                    yield return firstEnumerator.Current;
                }

                if (secondEnumerator.MoveNext())
                {
                    empty = false;
                    yield return secondEnumerator.Current;
                }
            } while (!empty);
        }

        /**
         * Interleaves multiple sequences.
         * This method is generalization of @c Interleave with two arguments. No sequence
         * in the list has to be finite, but their overall number must be finite. Null
         * sequences are not allowed too.
         */
        public static IEnumerable<T> Interleave<T>(
            params IEnumerable<T>[] sequences)
        {
            var enumerators = sequences
                .Select(seq => seq.GetEnumerator())
                .ToArray();

            bool empty = true;
            do
            {
                empty = true;
                foreach (var enumerator in enumerators)
                {
                    if (enumerator.MoveNext())
                    {
                        empty = false;
                        yield return enumerator.Current;
                    }
                }
            } while (!empty);
        }
    }
}