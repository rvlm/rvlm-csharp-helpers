﻿using System;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * 
     */
    public static class CSharpStringHelper
    {
        /**
         * 
         */
        public static string EscapeCSharpString(this string str)
        {
            // Preallocate space up to 120% of the initial string size, because escaping
            // is likely to make the string little longer.
            // TODO: Extract magic number 12 / 10 into separate constant.
            StringBuilder sbuilder = new StringBuilder(str.Length * 12 / 10);
            foreach (char c in str)
            {
                switch (c)
                {
                    case '\'': sbuilder.Append("\\\'"); break;
                    case '\"': sbuilder.Append("\\\""); break;
                    case '\\': sbuilder.Append("\\\\"); break;
                    case '\0': sbuilder.Append("\\0");  break;
                    case '\a': sbuilder.Append("\\a");  break;
                    case '\b': sbuilder.Append("\\b");  break;
                    case '\f': sbuilder.Append("\\f");  break;
                    case '\n': sbuilder.Append("\\n");  break;
                    case '\r': sbuilder.Append("\\r");  break;
                    case '\t': sbuilder.Append("\\t");  break;
                    case '\v': sbuilder.Append("\\v");  break;
                    default:
                        if (!char.IsControl(c)) sbuilder.Append(c);
                        else sbuilder.Append("\\u{0:x4}".Fmt((ushort)c));
                        break;
                }
            }

            return sbuilder.ToString();
        }

        /**
         * 
         */
        public static string UnescapeCSharpString(this string str)
        {
            StringBuilder sbuilder = new StringBuilder(str.Length);
            for (int i=0; i<str.Length; ++i)
            {
                char c = str[i];
                if (c != '\\')
                    sbuilder.Append(c);
                else {
                    if (i+1 >= str.Length)
                        throw new FormatException();

                    char n = str[i+1];
                    switch (n) {
                        case '\'': sbuilder.Append('\''); break;
                        case '\"': sbuilder.Append('\"'); break;
                        case '\\': sbuilder.Append('\\'); break;
                        case '0':  sbuilder.Append('\0'); break;
                        case 'a':  sbuilder.Append('\a'); break;
                        case 'b':  sbuilder.Append('\b'); break;
                        case 'f':  sbuilder.Append('\f'); break;
                        case 'n':  sbuilder.Append('\n'); break;
                        case 'r':  sbuilder.Append('\r'); break;
                        case 't':  sbuilder.Append('\t'); break;
                        case 'v':  sbuilder.Append('\v'); break;
                        case 'x':
                        case 'u':
                            if (i+5 >= str.Length)
                                throw new FormatException();
                            
                            UInt16 codepoint = Convert.ToUInt16(str.Substring(i+2,4), 16);
                            sbuilder.Append(Convert.ToChar(codepoint));
                            i += 4;
                            break;
                        default:
                            throw new FormatException();
                    }
                    ++i;
                }
            }

            return sbuilder.ToString();
        }
    }
}
