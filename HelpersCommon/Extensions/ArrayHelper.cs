﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers.Extensions
{
    /**
     * 
     */
    public static class ArrayHelper
    {
        /**
         * 
         */
        public static T[] BindIndex0<T>(this T[,] array, int idx)
        {
            int len0 = array.GetLength(0);
            int len1 = array.GetLength(1);

            T[] result = new T[len1];
            for (int i = 0; i < len1; i++)
                result[i] = array[idx, i];

            return result;
        }

        /**
         * 
         */
        public static T[] BindIndex1<T>(this T[,] array, int idx)
        {
            int len0 = array.GetLength(0);
            int len1 = array.GetLength(1);

            T[] result = new T[len0];
            for (int i = 0; i < len0; i++)
                result[i] = array[i, idx];

            return result;
        }
    }
}
