﻿using System;
using System.Collections.Generic;

namespace Rvlm.CSharpHelpers
{
    /**
     * Class for assertion checking.
     * Unlike built-in .NET asserts @c System.Diagnostics.Debug.Assert
     * and @c System.Diagnostics.Debug.Assert this class does not show user a nice dialog
     * with buttons, but just throws an exception. Moreover, its checks are not compiled
     * out in release builds. Instead, it was designed to be a replacement of verbose
     * @c if-throw statements. Compare the following piece of code
     * @code
     *     public void AddBinding(
     *         object source,
     *         Control control,
     *         PropertyInfo sourceProperty,
     *         PropertyInfo controlProperty)
     *     {
     *         if (source != null)
     *             throw new ArgumentNullException("source");
     *             
     *         if (control != null)
     *             throw new ArgumentNullException("control");
     *             
     *         if (sourceProperty != null)
     *             throw new ArgumentNullException("sourceProperty");
     *             
     *         if (controlProperty != null)
     *             throw new ArgumentNullException("controlProperty");
     *         ...
     *     }
     * @endcode
     * and how it looks with assertions:
     * @code
     *     public void AddBinding(
     *         object source,
     *         Control control,
     *         PropertyInfo sourceProperty,
     *         PropertyInfo controlProperty)
     *     {
     *         Assert.NotNull(source);
     *         Assert.NotNull(control);
     *         Assert.NotNull(sourceProperty);
     *         Assert.NotNull(controlProperty);
     *         ...
     *     }
     * @endcode
     * 
     * @see AssertionFailedException
     */
    public static class Assert
    {
        /**
         * Default exception message which used when no custom message is explicitly
         * specified. Not very meaningful though.
         */
        public const string DefaultMessage = "Assertion failed";

        /**
         * Throws exception with a given @a message when @a assertion condition isn't met.
         * @see AssertionFailedException
         */
        public static void That(bool assertion, string message)
        {
            Assert.IsTrue(assertion, message);
        }

        /**
         * Throws exception with a given @a message when @a val isn't the @c true value.
         * @see AssertionFailedException
         */
        public static void IsTrue(bool val, string message)
        {
            if (!(val == true))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with a given @a message when @a val isn't a @c false value.
         * @see AssertionFailedException
         */
        public static void IsFalse(bool val, string message)
        {
            if (!(val == false))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with a given @a message when @a obj isn't a @c null value.
         * @see AssertionFailedException
         */
        public static void IsNull(object obj, string message)
        {
            if (!(obj == null))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with a given @a message when @a obj is a @c null value.
         * @see AssertionFailedException
         */
        public static void NotNull(object obj, string message)
        {
            if (!(obj != null))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with a given @a message when @a str is @c null or an empty
         * string.
         * @see AssertionFailedException
         */
        public static void NotNullOrEmpty(string str, string message)
        {
            if (string.IsNullOrEmpty(str))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with a given @a message when @a collection is @c null or
         * have no items.
         * @see AssertionFailedException
         */
        public static void NotNullOrEmpty<T>(IEnumerable<T> collection, string message)
        {
            if (collection == null || IsEmpty(collection))
                throw new AssertionFailedException(message);
        }

        /**
         * Throws exception with the default message when @a assertion condition
         * isn't met.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void That(bool assertion)
        {
            Assert.That(assertion, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a val isn't the @c true value.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void IsTrue(bool val)
        {
            Assert.IsTrue(val, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a val isn't a @c false value.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void IsFalse(bool val)
        {
            Assert.IsFalse(val, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a obj isn't a @c null value.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void IsNull(object obj)
        {
            Assert.IsNull(obj, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a obj is a @c null value.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void NotNull(object obj)
        {
            Assert.NotNull(obj, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a str is @c null or an empty
         * string.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void NotNullOrEmpty(string str)
        {
            Assert.NotNullOrEmpty(str, DefaultMessage);
        }

        /**
         * Throws exception with the default message when @a collection is @c null or
         * have no items.
         * @see AssertionFailedException
         * @see DefaultMessage
         */
        public static void NotNullOrEmpty<T>(IEnumerable<T> collection)
        {
            Assert.NotNullOrEmpty<T>(collection, DefaultMessage);
        }

        /**
         * @internal
         * Checks if given collection is empty by trying iterating over it.
         * No null checks should be done in that method as they must be performed
         * by the caller.
         */
        private static bool IsEmpty<T>(IEnumerable<T> collection)
        {
            bool empty = true;
            foreach (var val in collection)
            {
                empty = false;
                break;
            }

            return empty;
        }
    }
}
