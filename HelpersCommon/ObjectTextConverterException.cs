﻿using System;
using System.Runtime.Serialization;

namespace Rvlm.CSharpHelpers
{
    [Serializable]
    public class ObjectTextConverterException : Exception
    {
        public ObjectTextConverterException() { }

        public ObjectTextConverterException(string message)
            : base(message) { }

        public ObjectTextConverterException(string message, Exception inner)
            : base(message, inner) { }

        protected ObjectTextConverterException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }
}
