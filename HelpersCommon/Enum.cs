﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rvlm.CSharpHelpers
{
    /**
     * Усовершенствованный класс @c Enum.
     * 
     * Содержит набор обобщенных методов для упрощения работы с перечислениями. Код этого
     * класса позаимствован в интернете по следующему адресу:
     * 
     * Strongly typed replacement for @c System.Enum static class.
     * Contains several generic methods for simplifying operations with enums, like
     * parsing, formatting, enumeration and so on. The original code has been borrowed
     * from
     * http://blog.logrythmik.com/post/Better-Enum-Parsing-using-Strongly-Typed-Generics.aspx
     */
    public static class Enum<T>
        where T : struct
    {
        /**
         * Monadic-style case-sensitive convenience overload.
         * @see TryParse(string, T, bool)
         */
        public static T? TryParse(string name)
        {
            return TryParse(name, false);
        }

        /**
         * Monadic-style convenience overload with @a ignoreCase parameter.
         * @see TryParse(string, T, bool)
         */
        public static T? TryParse(string name, bool ignoreCase)
        {
            T value;
            if (!string.IsNullOrEmpty(name) && TryParse(name, out value, ignoreCase))
                return value;
            return null;
        }

        /**
         * Case-sensitive convenience overload.
         * @see TryParse(string, T, bool)
         */
        public static bool TryParse(string name, out T value)
        {
            return TryParse(name, out value, false);
        }

        /**
         * Tries to parse string @a name into value of enumeration @c T.
         * This function returns @c true on successful parsing, @c false otherwise. If
         * @a isnoreCase is set to @c true, case-insensitive parsing is performed,
         * case-sensitive otherwise. Result value is returned in @a value parameter.
         */
        public static bool TryParse(string name, out T value, bool ignoreCase)
        {
            try
            {
                value = Parse(name, ignoreCase);
                return true;
            }
            catch (ArgumentException)
            {
                value = default(T);
                return false;
            }
        }

        /** Wraps @c System.Enum.GetValues static method. */
        public static IList<T> GetValues()
        {
            var untypedArray = Enum.GetValues(typeof(T));
            var typedArray = new T[untypedArray.Length];
            for (int i = 0; i < untypedArray.Length; i++)
                typedArray[i] = (T)untypedArray.GetValue(i);

            return typedArray;
        }

        /** Wraps @c System.Enum.Parse static method. */
        public static T Parse(string name)
        {
            return Parse(name, false);
        }

        /** Wraps @c System.Enum.Parse static method with @a ignoreCase parameter. */
        public static T Parse(string name, bool ignoreCase)
        {
            return (T)Enum.Parse(typeof(T), name, ignoreCase);
        }

        /** Wraps @c System.Enum.Format static method. */
        public static string Format(object value, string format)
        {
            return Enum.Format(typeof(T), value, format);
        }

        /** Wraps @c System.Enum.GetName static method. */
        public static string GetName(object value)
        {
            return Enum.GetName(typeof(T), value);
        }

        /** Wraps @c System.Enum.GetNames static method. */
        public static IList<string> GetNames()
        {
            return Enum.GetNames(typeof(T));
        }

        /** Wraps @c System.Enum.GetUnderlyingType static method. */
        public static Type GetUnderlyingType()
        {
            return Enum.GetUnderlyingType(typeof(T));
        }

        /** Wraps @c System.Enum.IsDefined static method. */
        public static bool IsDefined(object value)
        {
            return Enum.IsDefined(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c object parameter. */
        public static T ToObject(object value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c byte parameter. */
        public static T ToObject(byte value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c sbyte parameter. */
        public static T ToObject(sbyte value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c int parameter. */
        public static T ToObject(int value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c uint parameter. */
        public static T ToObject(uint value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c long parameter. */
        public static T ToObject(long value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c object parameter. */
        public static T ToObject(ulong value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c ulong parameter. */
        public static T ToObject(short value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }

        /** Wraps @c System.Enum.ToObject static method with @c ushort parameter. */
        public static T ToObject(ushort value)
        {
            return (T)Enum.ToObject(typeof(T), value);
        }
    }
}
