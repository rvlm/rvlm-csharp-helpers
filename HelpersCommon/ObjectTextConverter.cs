﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Rvlm.CSharpHelpers.Extensions;

namespace Rvlm.CSharpHelpers
{
    /**
     * Simple class for reversible string convertion for basic data types.
     * This class is not about serializing any data type, instead it is focused on
     * conversion for only small set of basic types commonly met in .NET Framework.
     * Below is the complete list of supported data types, with examples:
     * 
     *   - @c bool (@c true, @c false);
     *   - @c int, @c long, and @c short (@c 42, @c -73);
     *   - @c uint, @c ulong, and @c ushort (@c 0, @c 42);
     *   - @c float and @c double (@c -1.013E-04);
     *   - @c char (@c 'a', @c '\n');
     *   - @c string (@c "A43", @c "\n\"");
     *   - @c DateTime
     *   - @c Version
     *   - enums;
     *   - @c Nullable of abovementioned;
     *   - @c IList of abovementioned.
     * 
     * The conversion is always reversible and lossless: value is parsed back exactly
     * as it was. This makes this class useful for storing data in configuration files
     * or passing through the network.
     */
    public static class ObjectTextConverter
    {
        public const string NullRepresentation  = "null";
        public const char ListOpeningBracket    = '[';
        public const char ListClosingBracket    = ']';
        public const char ListItemDelimiter     = ',';
        public const char StringQuotation       = '\"';
        public const char CharQuotation         = '\'';

        /**
         * @internal
         * Helper static field to make the below code shorter.
         */
        private static CultureInfo InvariantCulture = CultureInfo.InvariantCulture;

        /**
         * @internal
         * Dictionary for primitive data parsers. Keys are types, values are functions
         * which can parse this type from string. Tedious, but not a rocket science.
         * Static field is okay for multithreading because the dictionary is read-only.
         */
        private static Dictionary<Type,Func<string,object>> msPrimitiveParsers =
            new Dictionary<Type, Func<string,object>> {
            { typeof(bool),     s => bool   .Parse(s)                    },
            { typeof(int),      s => int    .Parse(s, InvariantCulture)  },
            { typeof(long),     s => long   .Parse(s, InvariantCulture)  },
            { typeof(short),    s => short  .Parse(s, InvariantCulture)  },
            { typeof(uint),     s => uint   .Parse(s, InvariantCulture)  },
            { typeof(ulong),    s => ulong  .Parse(s, InvariantCulture)  },
            { typeof(ushort),   s => ushort .Parse(s, InvariantCulture)  },
            { typeof(float),    s => float  .Parse(s, InvariantCulture)  },
            { typeof(double),   s => double .Parse(s, InvariantCulture)  },
            { typeof(char),     s => UnquoteChar(s)                      },
            { typeof(string),   s => UnquoteString(s)                    },
            { typeof(Version),  s => new Version(s)                      },
            { typeof(DateTime), s => DateTime.ParseExact(s, "O", InvariantCulture) },
        };

        /**
         * @internal
         * Dictionary for primitive data formatters. Keys are types, values are functions
         * which can format this type into string. Static field is okay for multithreading
         * because the dictionary is read-only.
         */
        private static Dictionary<Type,Func<object, string>> msPrimitiveFormatters = 
            new Dictionary<Type, Func<object,string>>{
            { typeof(bool),     obj => ((bool)obj)     .ToString()                      },
            { typeof(int),      obj => ((int)obj)      .ToString(InvariantCulture)      },
            { typeof(long),     obj => ((long)obj)     .ToString(InvariantCulture)      },
            { typeof(short),    obj => ((short)obj)    .ToString(InvariantCulture)      },
            { typeof(uint),     obj => ((uint)obj)     .ToString(InvariantCulture)      },
            { typeof(ulong),    obj => ((ulong)obj)    .ToString(InvariantCulture)      },
            { typeof(ushort),   obj => ((ushort)obj)   .ToString(InvariantCulture)      },
            { typeof(float),    obj => ((float)obj)    .ToString("R", InvariantCulture) },
            { typeof(double),   obj => ((double)obj)   .ToString("R", InvariantCulture) },
            { typeof(char),     obj => QuoteChar((char)obj)                             },
            { typeof(string),   obj => QuoteString((string)obj)                         },
            { typeof(Version),  obj => ((Version)obj)  .ToString()                      },
            { typeof(DateTime), obj => ((DateTime)obj) .ToString("O", InvariantCulture) },
        };
        
        /**
         * Parses the contents of given string @a str as a value of type @a type.
         * If argument @a type is @c null, @c ArgumentNullException is thrown. If any
         * other error occures, @c ObjectTextConverterException gets thowns instead. If
         * function successed, the result value is @em actually of the type @a type and
         * can be safely casted.
         */
        public static object ConvertFromString(string str, Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            try
            {
                // Check for primitive types at first, because they are supposed to be
                // the most frequent ones. More complicated cases go latter.
                Func<string, object> parser;
                if (msPrimitiveParsers.TryGetValue(type, out parser))
                    return parser(str);

                // Enums.
                if (type.BaseType == typeof(Enum))
                    return Enum.Parse(type, str, true);

                // Nullable<T>.
                Type nullableUnderlyingType = Nullable.GetUnderlyingType(type);
                if (nullableUnderlyingType != null)
                {
                    if (str == NullRepresentation)
                        return null;
                    
                    Type underlyingType = type.GetGenericArguments().First();
                    return ConvertFromString(str, underlyingType);
                }

                // IList<T>.
                Type listInterfaceType = GetListInterfaceType(type);
                if (listInterfaceType != null)
                {
                    if (str == NullRepresentation)
                        return null;

                    Type listItemType = listInterfaceType.GetGenericArguments().First();
                    var result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                    var addMethod = GetListAddMethod(type, listInterfaceType);

                    foreach (string s in ArrayTrickySplit(str))
                    {
                        object item = ConvertFromString(s, listItemType);
                        addMethod.Invoke(result, new object[] { item });
                    }

                    return result;
                }

                // If the above didn't help, no last resort rests.
                throw new ObjectTextConverterException("Cannot find conversion.");
            }
            catch (Exception exception)
            {
                if (exception is ArgumentException ||
                    exception is ArgumentOutOfRangeException ||
                    exception is OverflowException ||
                    exception is FormatException ||
                    exception is InvalidCastException)
                    throw new ObjectTextConverterException(
                        "Cannot parse value from string.", exception);
                throw;
            }
        }

        /**
         * Converts given value @a val of type @a type into string.
         * If argument @a type is @c null, @c ArgumentNullException is thrown. If any
         * other error occures, @c ObjectTextConverterException gets thowns instead.
         */
        public static string ConvertToString(object val, Type type)
        {
            if (type == null)
                throw new ArgumentNullException();

            try
            {
                Func<object, string> formatter;
                if (msPrimitiveFormatters.TryGetValue(type, out formatter))
                    return formatter(val);

                // Enums.
                if (type.BaseType == typeof(Enum))
                    return Enum.GetName(type, val);

                // Nullable<T>.
                Type nullableUnderlyingType = Nullable.GetUnderlyingType(type);
                if (nullableUnderlyingType != null)
                {
                    if (val == null)
                        return NullRepresentation;

                    return ConvertToString(
                        Convert.ChangeType(val, nullableUnderlyingType),
                        nullableUnderlyingType);
                }

                // IList<T>.
                Type listInterfaceType = GetListInterfaceType(type);
                if (listInterfaceType != null)
                {
                    if (val == null)
                        return NullRepresentation;

                    Type listItemType = listInterfaceType.GetGenericArguments().First();
                    string items = (val as IEnumerable)
                        .OfType<object>()
                        .Select(item => ConvertToString(item, listItemType))
                        .StringJoin(ListItemDelimiter + " ");

                    return ListOpeningBracket + items + ListClosingBracket;
                }
                
                throw new ObjectTextConverterException("Cannot find conversion.");
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (Exception exception)
            {
                if (exception is ArgumentException ||
                    exception is InvalidCastException)
                    throw new ObjectTextConverterException(
                        "Cannot convert value to string.", exception);
                throw;
            }
        }

        private static string QuoteChar(char c)
        {
            return CharQuotation + c.ToString().EscapeCSharpString() + CharQuotation;
        }

        private static char UnquoteChar(string s)
        {
            if (s.Length < 2 || s[0] != CharQuotation || s[s.Length-1] != CharQuotation)
                throw new FormatException("Char constant isn't properly quoted.");

            string unescaped = s.Substring(1, s.Length-2).UnescapeCSharpString();
            if (unescaped.Length != 1)
                throw new FormatException("Char constant is too long or too short.");

            return unescaped[0];
        }

        private static string QuoteString(string s)
        {
            return StringQuotation + s.EscapeCSharpString() + StringQuotation;
        }

        private static string UnquoteString(string s)
        {
            if (s.Length < 2 || s[0]!=StringQuotation || s[s.Length-1]!=StringQuotation)
                throw new FormatException("String constant isn't properly quoted.");

            return s.Substring(1, s.Length-2).UnescapeCSharpString();
        }

        private static string[] ArrayTrickySplit(string str)
        {
            string[] items = str.SplitKeepEmptyEntries(ListItemDelimiter);
            return items;
        }

        private static Type GetListInterfaceType(Type type)
        {
            return type
                .GetInterfaces()
                .FirstOrDefault(i => i.IsGenericType && 
                                i.GetGenericTypeDefinition() == typeof(IList<>));
        }

        public static MethodInfo GetListAddMethod(Type type, Type interfaceType)
        {
            var map = type.GetInterfaceMap(interfaceType);
            int idx = 0;
            foreach (var ifaceMethod in map.InterfaceMethods)
            {
                if (ifaceMethod.Name == "Add")
                    return map.TargetMethods[idx];

                ++idx;
            }

            throw new ObjectTextConverterException();
        }
    }
}
