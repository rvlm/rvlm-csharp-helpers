﻿using System;
using System.Reflection;

namespace Rvlm.CSharpHelpers
{
    /**
     * Helper class for retrieving assembly information.
     * This extract information which may be useful for "About" dialogs of some kind.
     * The code is based on Visual Studio 2008 Express Edition generated code for about
     * dialog, with lots of refactoring.
     */
    public sealed class AssemblyInfoHelper
    {
        /**
         * Constructs class object for given @a assembly.
         * @throw ArgumentNullException if @a assembly is @c null.
         */
        public AssemblyInfoHelper(Assembly assembly)
        {
            if (assembly == null)
                throw new ArgumentNullException("assembly");

            RetrieveInfo(assembly);
        }

        /** Assembly title. */
        public string Title { get; private set; }

        /** Assembly version. */
        public Version Version { get; private set; }

        /** Assembly description. */
        public string Description { get; private set; }

        /** Assembly product information. */
        public string Product { get; private set; }

        /** Assembly copyright. */
        public string Copyright { get; private set; }

        /** Assembly company information. */
        public string Company { get; private set; }

        /** 
         * @internal
         * Fills out all properties after investigating assembly metadata. 
         */
        private void RetrieveInfo(Assembly assembly)
        {
            var attrTitle   = GetFirstAttr<AssemblyTitleAttribute>(assembly);
            var attrDesc    = GetFirstAttr<AssemblyDescriptionAttribute>(assembly);
            var attrProduct = GetFirstAttr<AssemblyProductAttribute>(assembly);
            var attrCopy    = GetFirstAttr<AssemblyCopyrightAttribute>(assembly);
            var attrCompany = GetFirstAttr<AssemblyCompanyAttribute>(assembly);

            Title       = (attrTitle   == null) ? null : attrTitle.Title;
            Description = (attrDesc    == null) ? null : attrDesc.Description;
            Product     = (attrProduct == null) ? null : attrProduct.Product;
            Copyright   = (attrCopy    == null) ? null : attrCopy.Copyright;
            Company     = (attrCompany == null) ? null : attrCompany.Company;

            // The easy part.
            Version = assembly.GetName().Version;
        }

        /**
         * @internal
         * Looks through assembly custom attributes and returns the first of type @c T,
         * or @c null if not such attribute found.
         */
        private static T GetFirstAttr<T>(Assembly assembly)
            where T : Attribute 
        {
            object[] attrs = assembly.GetCustomAttributes(typeof(T), false);
            return (attrs.Length > 0) ? (T)attrs[0] : null;
        }
    }
}
