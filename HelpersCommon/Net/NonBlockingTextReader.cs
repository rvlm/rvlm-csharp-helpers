﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace Rvlm.CSharpHelpers.Net
{
    public sealed class NonBlockingTextReader
    {
        private Socket mSocket;
        private Encoding mEncoding;
        private Decoder mDecoder;
        private string mNewline;
        private string mRest;
        private byte[] mBuffer;
        private char[] mChars;

        public NonBlockingTextReader(Socket socket, Encoding encoding, string newline)
        {
            mSocket   = socket;
            mEncoding = encoding;
            mDecoder  = encoding.GetDecoder();
            mNewline  = newline;
            mBuffer   = new byte[socket.ReceiveBufferSize];
            mChars    = new char[socket.ReceiveBufferSize];
            mRest     = "";
            mSocket.Blocking = false;
            mDecoder.Fallback = DecoderFallback.ReplacementFallback;
        }

        public int TryReadLine(List<string> lines)
        {
            if (mSocket.Available <= 0)
                return 0;

            int size = mSocket.Receive(mBuffer);
            int len = mDecoder.GetChars(mBuffer, 0, size, mChars, 0);
            string str = mRest + new string(mChars, 0, len);

            int cutIndex = 0;
            int endIndex = 0;
            int count = 0;
            while (true)
            {
                endIndex = str.IndexOf(mNewline, cutIndex);
                if (endIndex <= 0)
                    break;

                lines.Add(str.Substring(cutIndex, endIndex - cutIndex + mNewline.Length));
                cutIndex = endIndex + mNewline.Length;
                count++;
            }
            
            mRest = str.Substring(cutIndex);
            return count;
        }

    }
}
